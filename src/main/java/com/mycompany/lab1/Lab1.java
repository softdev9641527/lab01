/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class Lab1 {
    static char turn = 'X';
    static String WinCheck = "Draw";

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String[][] arr = {
                {"-", "-", "-"},
                {"-", "-", "-"},
                {"-", "-", "-"}};
        System.out.println("Welcome to Program XO");
        char end = 0;
        
        while (end == 0){ //X
            if (CheckHorizontal(arr)) {
                end = 1;
                break;
            } else {
                if (CheckVertical(arr)) {
                    end = 1;
                    break;
                } else if (CheckLeftOblique(arr)) {
                    end = 1;
                    break;
                } else if (CheckRightOblique(arr)) {
                    end = 1;
                    break;
                } else if (CheckDraw(arr) == 9) {
                    Table(arr);
                    end = 1;
                    break;
                } 
            }
            if (turn == 'X') {
                Table(arr);
                System.out.println("Turn X");
                System.out.println("Please input row:");
                System.out.println("Please input col:");
                int x = kb.nextInt();
                int y = kb.nextInt();
                
                TableX(x, y, arr);
                
            } else { //O
                Table(arr);
                System.out.println("turn O");
                System.out.println("Please input row:");
                System.out.println("Please input col:");
                int x = kb.nextInt();
                int y = kb.nextInt();
                
                TableO(x, y, arr);
            }
        }
        WinnerCheck();
    }
    
    private static void WinnerCheck() {
        if (WinCheck.equals("X")) {
            System.out.println("Player X Win");
    
        }else if (WinCheck.equals("O")) {
            System.out.println("Player O Win");
            
        }else {
            System.out.println("It's a draw");
        }
    }
    
    private static int CheckDraw(String[][] arr) {
        int count = 0;
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                if (!arr[i][j].equals("-")) {
                    count++;
                }
            }
        }
        return count;
    }
    
    private static void TableX(int x, int y, String[][] arr) {
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                if (x-1 == i && y-1 ==j && arr[i][j].equals("-")) {
                    arr[i][j] = "X";
                    turn = 'O';
                    break;
                }
            }
        }
    }
    
        private static void TableO(int x, int y, String[][] arr) {
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                if (x-1 == i && y-1 ==j && arr[i][j].equals("-")) {
                    arr[i][j] = "O";
                    turn = 'X';
                    break;
                }
            }
        }
    }
        
    private static boolean CheckLeftOblique(String[][] arr) {
        if(arr[0][0].equals(arr[1][1]) && arr[1][1].equals(arr[2][2]) && !arr[0][0].equals("-")) {
            WinCheck = arr[0][0];
            Table(arr);
            return true;
        }
        return false;
    }
    
    private static boolean CheckRightOblique(String[][] arr) {
        if(arr[0][2].equals(arr[1][1]) && arr[1][1].equals(arr[2][0]) && !arr[0][2].equals("-")) {
            WinCheck = arr[0][2];
            Table(arr);
            return true;
        }
        return false;
    }
        
    private static boolean CheckHorizontal(String[][] arr) { //row
        for (int i=0; i<3; i++) {
            if (arr[i][0].equals(arr[i][1]) && arr[i][1].equals(arr[i][2])&& !arr[i][0].equals("-")) {
                WinCheck = arr[i][0];
                Table(arr);
                return true;
            }
        }
        return false;
    }
    
    private static boolean CheckVertical(String[][] arr) { //col
        for (int i=0; i<3; i++) {
            if (arr[0][i].equals(arr[1][i]) && arr[1][i].equals(arr[2][i])&& !arr[0][i].equals("-")) {
                WinCheck = arr[0][i];
                Table(arr);
                return true;
            }
        }
        return false;
    }
    
    private static void Table(String[][] arr) {
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
